from django.db.models import Sum
from .models import WordCountModel
from rest_framework import generics
from wordcount.serializers import WordCountSerializer


NUMBER_OF_RESULTS = 10


class StatsViewSet(generics.ListAPIView):
    """
    API endpoint that lists 10 most frequent words in TEONITE articles.
    """
    serializer_class = WordCountSerializer

    def get_queryset(self):
        queryset = WordCountModel.objects.all()
        author = self.kwargs.get('author', None)

        # Filter if author was specified in the url
        if author is not None:
            queryset = queryset.filter(author=author)

        # Group by 'word', aggregate count for unique groups, sort descending, return NUMBER_OF_RESULTS
        return queryset.values('word').annotate(wordsum=Sum('count')).order_by('-wordsum')[:NUMBER_OF_RESULTS]
