from django.db import models


class WordCountModel(models.Model):
    """
    Model for word count in an article written by an author.
    """
    word = models.CharField(max_length=30)
    author = models.CharField(max_length=30)
    count = models.IntegerField()
