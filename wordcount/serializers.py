from .models import WordCountModel
from rest_framework import serializers


class WordCountSerializer(serializers.ModelSerializer):
    """
    Simple serializer for word count.
    """
    wordsum = serializers.IntegerField()

    class Meta:
        model = WordCountModel
        fields = ('word', 'wordsum')
