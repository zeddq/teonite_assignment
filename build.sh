#!/usr/bin/env bash

# Start scrapy deamon
cd scraper
rm -f twistd.pid
(scrapyd &)
cd -

# Update DB and run server
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py runserver 0.0.0.0:8080