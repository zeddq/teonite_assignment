# -*- coding: utf-8 -*-
from scrapy.spiders import CrawlSpider


class BlogCrawlerSpider(CrawlSpider):
    name = 'blogcrawler'

    allowed_domains = ['teonite.com']
    start_urls = ['https://teonite.com/blog/']
    custom_settings = {
        'RETRY_ENABLED': True,
        'DEPTH_LIMIT': 0,
        'DEPTH_PRIORITY': 1,
        'LOG_ENABLED': False,
        'LOG_LEVEL': 'DEBUG',
        'CONCURRENT_REQUESTS_PER_DOMAIN': 32,
        'CONCURRENT_REQUESTS': 32,
    }

    def parse(self, response):
        # Parse the articles
        for article in response.css("h2.post-title a"):
            yield response.follow(article, callback=self.parse_article)

        # Go to the next page if there is one
        next_pages = response.css("a.older-posts")
        if len(next_pages) > 0:
            yield response.follow(next_pages[0], callback=self.parse)

    def parse_article(self, response):
        def get_content_from_query(query):
            return response.css(query).extract()

        # Extract author and content
        author = response.css("span.author-content h4::text").extract_first().replace(" ", "").lower()

        content = get_content_from_query("section.post-content p::text")
        content.extend(get_content_from_query("section.post-content li::text"))
        content.extend(get_content_from_query("section.post-content h3::text"))

        yield {
            'author': author,
            'content': content,
        }
