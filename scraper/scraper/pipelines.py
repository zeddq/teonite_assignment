# -*- coding: utf-8 -*-
import string
import re
import unicodedata
from wordcount.models import WordCountModel
from collections import Counter


class StoringPipeline(object):
    def process_item(self, item, spider):

        # Normalize string
        author = item['author'].replace("ł", "l")
        author = unicodedata.normalize('NFKD', author).encode('ASCII', 'ignore').decode("utf-8")

        # Extract words from the content
        words = []
        for s in item['content']:
            words.extend([re.sub('^[{0}]+|[{0}]+$'.format(string.punctuation), '', w) for w in s.split()])

        wordcounts = Counter(words)
        wordcounts.pop('', None)

        # Store (words, author, count) from one article
        for word, count in wordcounts.items():
            wordcount = WordCountModel()
            wordcount.author = author
            wordcount.word = word
            wordcount.count = count
            wordcount.save()

        return item
