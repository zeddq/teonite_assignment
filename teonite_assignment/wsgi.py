"""
WSGI config for teonite_assignment project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os
from scrapyd_api import ScrapydAPI

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'teonite_assignment.settings')

# Send a request to scrapyd once on startup
scrapyd_object = ScrapydAPI('http://localhost:6800')
scrapyd_object.schedule('default', 'blogcrawler')

application = get_wsgi_application()
